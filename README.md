# NuNet Bootstrap Node

This repository contains code for the NuNet Libp2p network bootstrap servers.

The following nodes are currently in use:
```
/dnsaddr/bootstrap.p2p.nunet.io/p2p/QmQ2irHa8aFTLRhkbkQCRrounE4MbttNp8ki7Nmys4F9NP
/dnsaddr/bootstrap.p2p.nunet.io/p2p/Qmf16N2ecJVWufa29XKLNyiBxKWqVPNZXjbL3JisPcGqTw
/dnsaddr/bootstrap.p2p.nunet.io/p2p/QmTkWP72uECwCsiiYDpCFeTrVeUM9huGTPsg3m6bHxYQFZ
```

## Building

To build the server use golang version 1.19.x
```
go build -o bootstrap-node
```


## Running

A JSON configuration file can be used to specify the following parameters
- **listen_addr** : List of listening addresses for the node
- **bootstrap_peers** : Peers for the node to boostrap from
- **private_key_file** : File path to store private key
- **public_key_file** : File path to store public key

When running the program, a path to the configuration file can be supplied with `-config` flag.
